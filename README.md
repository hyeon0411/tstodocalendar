2020년 캘린더와 투두리스트<br>
제공한 gif 파일을 참조하여 캘린더와 투두리스트를 만드시면 됩니다.<br>
디자인은 gif과 똑같지 않아도 됩니다.<br>

캘린더에는 이하의 요소가 포함되어야 합니다:<br>

년도 (2020년으로 고정)<br>
선택한 달<br>
1월을 초기값으로 설정<br>
다음달 또는 이전달로 이동하는 화살표 버튼 2개 (1월이거나 12월이면 왼쪽 또는 오른쪽 버튼 비활성화)<br>
달력<br>
날짜를 선택하면 칸 색이 바뀜<br>
투두리스트에는 이하의 요소가 포함되어야 합니다:<br>

선택한 날짜의 일과 요일<br>
할 일 목록<br>
각 목록에는 시간, 할 일 제목, 수정 버튼과 삭제 버튼이 있음<br>
수정 버튼을 누르면 시간과 할 일 제목을 수정하는 폼을 띄움<br>
삭제 버튼을 누르면 삭제<br>
아래쪽에는 추가 버튼과 모두 삭제 버튼이 존재<br>
추가 버튼을 누르면 시간과 할 일 제목을 추가하는 폼을 띄움<br>
모두 삭제를 누르면 추가했던 모든 할 일을 삭제함<br>
초기에는 선택한 날짜가 없으므로, 일과 요일을 띄우지 않고 추가 버튼과 모두 삭제 버튼을 비활성화<br>
시간 추가 및 수정시에는 24시간 형식의(00:00~23:59) HH:MM 형식만 입력 가능하도록 유효성을 체크하세요.<br>
redux를 이용하여 선택한 일자 및 할 일 목록을 관리하세요.<br>
<br>
완성 뒤 실행에 필요한 파일들을 zip으로 압축하여 저한테 보내주세요.
