
export const ADD_TODO = 'ADD_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const ALL_CLEAR_BY_DATE = 'ALL_CLEAR_BY_DATE';

export function addTodo(todo : any) {
    return {
        type: ADD_TODO,
        payload: todo,
    }
}

export function deleteTodo(todoId : any) {
    return {
        type: DELETE_TODO,
        payload: todoId,
    }
}

export function updateTodo(todo : any) {
    return {
        type: UPDATE_TODO,
        payload: todo,
    }
}

export function allClearByDate(todo : any) {
    return {
        type: ALL_CLEAR_BY_DATE,
        payload: todo,
    }
}
