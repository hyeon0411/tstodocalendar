import React, {Fragment, FC} from 'react';

import useCalendar, {Column} from '../hooks/useCalendar';
import TodoList from "./TodoList";

const MainContainer: FC = () => {
    const {calendarRows, selectedDate, todayFormatted, daysShort, monthNames, getNextMonth, getPrevMonth, setSelectedDate} = useCalendar();

    function convertDDMMYYYtoDate(DDMMYYYY: string): Date {
        const [day, month, year] = DDMMYYYY.split("-")
        return new Date(`${year}-${month}-${day}`)
    }

    const dateClickHandler = (DDMMYYYY: string) => {
        setSelectedDate(convertDDMMYYYtoDate(DDMMYYYY))
    }

    return (
        <Fragment>
            <p>Selected Date : {`${selectedDate.toDateString()}`}</p>
            <table className="table">
                <thead>
                <tr>
                    {daysShort.map(day => (
                        <th key={day}>{day}</th>
                    ))}
                </tr>
                </thead>
                <tbody>
                {
                    Object.values(calendarRows).map((cols: Column[]) => {
                        return <tr key={cols[0].date}>
                            {cols.map(col => (
                                <td key={col.date} onClick={() => dateClickHandler(col.date)} className={`
                                ${"date"} 
                                ${col.classes}
                                ${convertDDMMYYYtoDate(col.date).toDateString() === selectedDate.toDateString() ? "selected" : ""}
                                ${col.date === todayFormatted ? "today" : ""}
                                `}>{col.value}</td>
                            ))}
                        </tr>
                    })
                }
                </tbody>
            </table>

            <button className="button" onClick={getPrevMonth}>Prev</button>
            <button className="button" onClick={getNextMonth}>Next</button>
            <TodoList selectedDate={selectedDate}/>

        </Fragment>
    );
}

export default MainContainer;
