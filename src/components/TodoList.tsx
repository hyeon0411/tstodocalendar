import React from "react";
import TodoItem, {ITodo} from "./TodoItem";
import {DefaultRootState, useSelector} from "react-redux";
import TodoInput from "./TodoInput";

function TodoList({selectedDate}: { selectedDate: Date }) {
    const todos = useSelector(state => {
        const todos = state as Array<ITodo>
        return todos.filter(todo=>
        todo.date.getMonth().toString() + todo.date.getDate().toString() ===
        selectedDate.getMonth().toString() + selectedDate.getDate().toString())
    }) as ITodo[]
    return (
        <div className='my-4'>
            <TodoInput selectedDate={new Date(selectedDate)}/>
            {todos.map(todo => {
                return <TodoItem key={todo.id} todo={todo} selectedDate={new Date(selectedDate)}/>
            })}
        </div>
    )
}

export default TodoList
