import React, {useState} from 'react'
import {useDispatch} from "react-redux";
import {deleteTodo, updateTodo} from "../redux/actions";

export interface ITodo {
    id: number;
    index: number;
    task: string;
    date: Date;
    yyyymmdd: string;
}

function TodoItem({todo, selectedDate}: { key: number, todo: ITodo, selectedDate: Date }) {
    const [editable, setEditable] = useState(false);
    const [task, setTask] = useState(todo.task);
    const [time, setTime] = useState(todo.date.getHours() + ":" + todo.date.getMinutes());

    const hhmm = time.split(':')
    const hh = Number(hhmm[0])
    const mm = Number(hhmm[1])

    const date = new Date(selectedDate)

    let dispatch = useDispatch();
    return (
        <div>
            <div className='row mx-2 align-items-center'>
                <div className='col'>{
                    editable ? <input
                            onChange={(e) => setTime(e.target.value)}
                            value={time}
                            type='time'
                            className='col form-control'/>
                        : <h5>{('0' + todo.date.getHours()).substr(-2) +
                        ':' + ('0' + todo.date.getMinutes()).substr(-2)}</h5>
                }
                </div>

                <div className='col'>
                    {editable ?
                        <input type='text'
                               className='form-control'
                               value={task}
                               onChange={
                                   (e) => setTask(e.target.value)}
                        /> :
                        <h5>{todo.task}</h5>}

                </div>
                <button
                    onClick={() => {
                        dispatch(updateTodo(
                            {
                                ...todo,
                                task,
                                date : new Date(selectedDate.setHours(hh, mm)),
                            },
                        ))
                        if (editable) {
                            setTask(todo.task);
                        }
                        setEditable(!editable);

                    }}
                    className='btn btn-primary mx-2'
                >{editable ? 'Update' : 'Edit'}
                </button>


                <button
                    onClick={() => dispatch(deleteTodo(todo.id))}
                    className='btn btn-danger m2'
                >Delete
                </button>
            </div>
            <br/>
        </div>
    )
}

export default TodoItem
