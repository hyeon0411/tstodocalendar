import React, {useState} from 'react'
import {addTodo, allClearByDate} from '../redux/actions';
import {v1 as uuid} from 'uuid'
import {useDispatch} from "react-redux";

export function formatDate(date: Date): string {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('');
}

function TodoInput({selectedDate}: { selectedDate: Date }) {
    let [task, setTask] = useState("")
    const [time, setTime] = useState('00:00')


    let dispatch = useDispatch();
    return (
        <div>
            <h3>Todo</h3>
            <div className='row m-2'>

                <input
                    onChange={(e) => setTime(e.target.value)}
                    value={time}
                    type='time'
                    className='col form-control'/>

                <input
                    onChange={(e) => setTask(e.target.value)}
                    value={task}
                    type='text'
                    className='col form-control'
                    placeholder={'Enter the task here'}/>

                <button
                    onClick={() => {
                        dispatch(addTodo(
                            {
                                id: uuid(),
                                task: task,
                                date: new Date(selectedDate.setHours(Number(time.slice(0, 2)), Number(time.slice(3, 5)))),
                                yyyymmdd: formatDate(selectedDate)
                            }
                        ))
                        setTask('');
                    }}
                    className='btn btn-primary mx-2'>Add

                </button>

                <button
                    onClick={() => dispatch(allClearByDate(
                        {
                            yyyymmdd: formatDate(selectedDate)
                        }
                    ))
                    }
                    className='btn btn-danger m2'
                >Clear
                </button>
            </div>
        </div>
    )
}

export default TodoInput
