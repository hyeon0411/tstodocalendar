import React, {FC, Fragment} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import MainContainer from "./components/MainContainer";


const App: FC = () => {
    return (
        <Fragment>
            <section className="hero is-primary">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title has-text-centered">Calendar</h1>
                    </div>
                </div>
            </section>
            <div className="container has-text-centered">
                <MainContainer/>
            </div>
        </Fragment>
    );
}

export default App;
